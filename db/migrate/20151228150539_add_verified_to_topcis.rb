class AddVerifiedToTopcis < ActiveRecord::Migration
  def change
    add_column :topics, :verified, :boolean
  end
end

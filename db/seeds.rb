user = User.create({email: "somebody@something.com", encrypted_password: "$2a$10$zYg3y/P43jc6WwgALXzEU.jghrXoguoAoauCoMMeW.DBW4sxnRsai", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-12-14 12:38:41", last_sign_in_at: "2015-12-14 12:38:41", current_sign_in_ip: "::1", last_sign_in_ip: "::1", provider: nil, uid: nil, picture: nil, first_name: nil, last_name: nil, token: nil, token_expiry: nil})
other_user = User.create({email: "amiran12@hotmail.com", encrypted_password: "$2a$10$caTWqypSZjfi/HQETcr8Ze5tmQ107MPZOOdaHDwe0d9KiE6T2yRbq", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 4, current_sign_in_at: "2015-12-16 10:30:25", last_sign_in_at: "2015-12-16 10:29:55", current_sign_in_ip: "::1", last_sign_in_ip: "::1", provider: "facebook", uid: "10156313729035626", picture: "http://graph.facebook.com/10156313729035626/picture?type=large", first_name: "Ami", last_name: "Ya", token: "CAAHqYA9ndWIBAPJlDpUP9sH3FZBc7ZAZATWFZCczqvZCotZAyMuazf4Senthro2MqgXPSIUQhMRxCPLZBozXBc0GLtMxdBsgcQoymBcTNQ7eSU6yQRbjGj22R0npnOS7EB9lFqhOGGpqYswWvpMWnzbogHr68KnVifBS2xaAN2nOaEtAFKEM2bSfE9Du0nOR9sZD", token_expiry: "2016-02-14 09:51:39"})

topic = Topic.create!({name: "Geopolitics", user: user, picture_file_name: "geopolitics.png", picture_content_type: "image/png", picture_file_size: 90311, picture_updated_at: "2015-12-14 12:38:57"})
topic_two = Topic.create!( {name: "Tech", user: user, picture_file_name: "tech.png", picture_content_type: "image/png", picture_file_size: 103345, picture_updated_at: "2015-12-14 12:43:09"})
topic_three = Topic.create!({name: "design", user: user, picture_file_name: "design.png", picture_content_type: "image/png", picture_file_size: 403506, picture_updated_at: "2015-12-14 12:45:05"})
topic_four = Topic.create!({name: "Development", user: user, picture_file_name: "development.png", picture_content_type: "image/png", picture_file_size: 78907, picture_updated_at: "2015-12-14 12:53:58"})


article = Article.create!(title: "Economic troubles in South Africa", url: "http://www.theguardian.com/world/2015/dec/13/president-zuma-hires-south-africas-third-finance-chief-in-a-week", description: "", votes: 1, user: user, topic: topic)
article = Article.create!(title: "The tech bubble about to burst", url: "http://www.wired.com/2015/12/if-the-unicorn-bubble-bursts-workers-will-feel-it-the-worst/", description: "", votes: 1, user: user, topic: topic)
article = Article.create!(title: "Troubles in China", url: "www.caixin.cn", description: "", votes: 0, user: user, topic: topic)

Articlevote.create!({user: user, article: article})
Articlevote.create!(  {user: user, article: article} )

# {article_id: nil, content: "blabla", user_id: nil},
# {article_id: 1, content: "dfgdgfdfg", user_id: 1},
# {article_id: 1, content: "sdfsdfwef", user_id: 1},
# {article_id: 1, content: "rertert", user_id: 1},
# {article_id: 1, content: "rtyrty", user_id: 1},
# {article_id: 1, content: "sdfsdfsdfsdf", user_id: 1},
# {article_id: 3, content: "dfsdfdsf", user_id: 1},
# {article_id: 3, content: "", user_id: 1},
# {article_id: 1, content: "sdfsdfsdf", user_id: 1},
# {article_id: 1, content: "sdf", user_id: 1},
# {article_id: 3, content: "amiran", user_id: 1},
# {article_id: 1, content: "Amiran", user_id: 1},
# {article_id: 2, content: "Hello", user_id: 1},
# {article_id: 2, content: "What happens if I write a really long comment. Like super duper long. so long", user_id: 1}

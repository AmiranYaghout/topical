class Article < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic
  has_many   :comments

  validates_presence_of :user
  validates_presence_of :topic
  validates :title, presence: { message: "seems to be empty."},
            length: {minimum: 8}
  validates_format_of :url, with: URI::regexp(%w(http https)), multiline: true, message: "seems to be invalid"
  validates :description, length: {minimum:10, maximum: 200}, allow_blank: true

  def set_user!(user)
    self.user_id = user.id
    self.save
  end
end



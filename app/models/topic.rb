class Topic < ActiveRecord::Base
  belongs_to :user
  has_many :articles
   include PgSearch
  pg_search_scope :search_by_name_and_description, against: [ :name, :description ]

  has_attached_file :picture,
    styles: { medium: "300x300>", thumb: "100x100>" }

  validates_presence_of :user
  validates :name, presence: { message: "seems to be empty."},
            length: {minimum: 2}
  validates :description, length: {minimum:10, maximum: 200}, allow_blank: true
  validates_attachment_content_type :picture,
    content_type: /\Aimage\/.*\z/


  def set_user!(user)
    self.user_id = user.id

    self.save
  end
end


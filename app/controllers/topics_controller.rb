class TopicsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

def index
  @topics = policy_scope(Topic)
end

def show
  @topic = Topic.find(params[:id])
end


def new
@topic = Topic.new
authorize @topic
end

def create
  @topic = Topic.new(topic_params)
  @topic.set_user!(current_user)
  authorize @topic
    if @topic.save
      respond_to do |format|
        format.js
        format.html {redirect_to root_path}
        end
        flash[:notice] = 'Thank you, we will be in touch soon'
    else
      respond_to do |format|
        format.js { render }
        format.html { render :new }
      end
  end
end

def edit
  @topic = Topic.find(params[:id])
  authorize @topic
end

def update
  @topic = Topic.find(params[:id])
  @topic.update(topic_params)
  authorize @topic
  if @topic.save
    flash[:notice] = "Your topic has been updated"
     redirect_to topic_articles_path(@topic.id)
  else
    flash[:notice] = "Oops something went wrong"
  end
end



private

def topic_params
params.require(:topic).permit(:name,:picture,:user_id, :description)
end



end

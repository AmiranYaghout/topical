class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :find_article

  def index
   @comments = scope_policy(Comment).where(article: @article)
  end

  def new
    @comment = Comment.new
    authorize @comment
  end

  def create
    @article_id = params[:article_id]
    @comment = @article.comments.build(comment_params)
    @comment.user = current_user
    authorize @comment
    if @comment.save
      respond_to do |format|
        format.js # no need to pass a variable here
        format.html
      end
    end
  end

  def edit
  end

  def destroy
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :user_id)
  end
  def find_article
    @article = Article.find(params[:article_id])
  end
end

class UserPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      scope.all
    end
  end


  def show?
    record == user
  end

  def create?
    true
  end

  def edit
    record == user
  end

  def update?
    record == user  # Only restaurant creator can update it
  end

  def destroy?
    record == user  # Only restaurant creator can update it
  end

end


class ArticlePolicy < ApplicationPolicy
  class Scope < Scope
    attr_accessor :topic
    def resolve
      scope.all
    end
  end

  def index
    true
  end

  def show?
    true  # Anyone can view a restaurant
  end

  def create?
    true  # Anyone can create a restaurant
  end

  def edit
    record.user == user
  end

  def update?
    record.user == user  # Only restaurant creator can update it
  end

  def destroy?
    record.user == user  # Only restaurant creator can update it
  end

  def vote?
    true
  end

  def find?
    true
  end
end
